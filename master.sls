salt:
  master_remove_config: True
  master:
    fileserver_backend:
      - git
    gitfs_remotes:
      - https://gitlab.com/demopg/top.git
      - git://github.com/saltstack-formulas/salt-formula.git
      - https://github.com/gilou/consul-formula.git:
        - base: firewalld
      - git://github.com/saltstack-formulas/consul-formula.git
      - git://github.com/gilou/barman-formula.git:
        - base: demo
      - git://github.com/saltstack-formulas/hostsfile-formula.git
      - https://github.com/gilou/postgres-formula.git:
        - base: repmgr
    ext_pillar:
        - git:
          - master https://gitlab.com/demopg/pillar.git
