mine_functions:
  ssh.host_keys:
    private: False
  ssh.user_keys:
    prvfile: False

postgres:
  repmgr:
    use_repmgrd: True
    exchange_ssh_keys: True
    monitoring_history: True
    synchronous_standby_names:
      - node1
      - node2
  add_profile: True
  prepare_cluster:
    run: False
  use_upstream_repo: True
  version: '10'
  fromrepo: pgdg10,base
  pkgs_extra:
    - repmgr10 # TODO: use the version from pillar or patch the formula
  acls:
    - ['local', 'peer', 'peer']
    - ['local', 'replication', 'peer']
    - ['host', 'replication', 'repmgr', '192.168.0.0/16', 'trust']
    - ['host', 'replication', 'streaming_barman', '192.168.0.0/16', 'md5']
    - ['host', 'repmgr', 'repmgr', '192.168.0.0/16', 'trust']
    - ['host', 'all', 'all', '192.168.0.0/16', 'md5']
  users:
    repmgr:
      ensure: present
      createdb: True
      createroles: True
      createuser: True
      inherit: True
      replication: True
  archive_command: "rsync -a %p barman@node4:/var/lib/barman/{{grains['id'] }}/incoming/%f"

consul:
  service: True
  firewalld_config: True
  version: 1.2.3
  config:
    encrypt: A8zRdaeePE34FOitYZhDPQ==
    server: True
    bind_addr: 0.0.0.0
    bootstrap_expect: 3
