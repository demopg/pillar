barman:
  use_upstream_repo: False
  hosts:
    node1:
      archiver: "on"
      description: "backup for node1"
      conninfo: "host=node1 user=barman dbname=postgres"
      backup_method: postgres
      streaming_conninfo: "host=node1 user=streaming_barman dbname=postgres"
      streaming_archiver: "on"
      slot_name: barman
      last_backup_maximum_age: "1 DAYS"
      retention_policy: REDUNDANCY 4
      cron:
        ensure: present
        hour: 14
        minute: 30
